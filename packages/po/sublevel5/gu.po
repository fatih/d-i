# translation of d-i.po to Gujarati
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Contributor:
# Kartik Mistry <kartik.mistry@gmail.com>, 2006-2011.
#
msgid ""
msgstr ""
"Project-Id-Version: d-i\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-23 20:01+0000\n"
"PO-Revision-Date: 2006-07-10 15:53+0530\n"
"Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>\n"
"Language-Team: \n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid "Use non-free firmware?"
msgstr "મુક્ત ન હોય તેવા ફર્મવેર વાપરશો?"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
#, fuzzy
#| msgid ""
#| "Some non-free software has been made to work with Debian. Though this "
#| "software is not at all a part of Debian, standard Debian tools can be "
#| "used to install it. This software has varying licenses which may prevent "
#| "you from using, modifying, or sharing it."
msgid ""
"Some non-free firmware has been made to work with Debian. Though this "
"firmware is not at all a part of Debian, standard Debian tools can be used "
"to install it. This firmware has varying licenses which may prevent you from "
"using, modifying, or sharing it."
msgstr ""
"કેટલાક મુક્ત ન હોય તેવા ફર્મવેર ડેબિયન સાથે કામ કરવા માટે બનાવવામાં આવ્યા છે. આ ફર્મવેર "
"ડેબિયનનાં ભાગરૂપ ન હોવા છતાં, ડેબિયનનાં પ્રમાણભૂત સાધનો તેને સ્થાપિત કરવા માટે વાપરી "
"શકાય છે. આ ફર્મવેરને અલગ લાયસન્સ છે જે તમને તેનો ઉપયોગ કરતાં, બદલતાં, અથવા વહેંચતા રોકી "
"શકે છે."

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:58001
#, no-c-format
msgid "ZFS pool %s, volume %s"
msgstr "ZFS પૂલ %s, કદ %s"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:60001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:61001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s), પાર્ટિશન #%s"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:60001
msgid ""
"Your boot partition has not been configured with the ext2 file system. This "
"is needed by your machine in order to boot. Please go back and use the ext2 "
"file system."
msgstr ""
"તમારુ બુટ પાર્ટિશન ext2 ફાઇલ સિસ્ટમ સાથે રૂપરેખાંકિત કરેલ નથી. આ તમારા મશીનને શરુ કરવા "
"માટે જરુરી છે. મહેરબાની કરી પાછા જાવ અને ext2 ફાઇલ સિસ્ટમ વાપરો."

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:61001
msgid ""
"Your boot partition is not located on the first partition of your hard disk. "
"This is needed by your machine in order to boot.  Please go back and use "
"your first partition as a boot partition."
msgstr ""
"તમારુ બુટ પાર્ટિશન તમારી હાર્ડ ડિસ્કનાં પ્રથમ પાર્ટિશનમાં નથી.  આ તમારા મશીનને શરુ કરવા "
"માટે જરુરી છે.  મહેરબાની કરી પાછા જાવ અને તમારા પ્રથમ પાર્ટિશનને બૂટ પાર્ટિશન તરીકે "
"ઉપયોગ કરો."

#. Type: text
#. Description
#. :sl5:
#. Setting to reserve a small part of the disk for use by BIOS-based bootloaders
#. such as GRUB.
#: ../partman-partitioning.templates:32001
msgid "Reserved BIOS boot area"
msgstr "આરક્ષિત BIOS બૂટ વિસ્તાર:"

#. Type: text
#. Description
#. :sl5:
#. short variant of 'Reserved BIOS boot area'
#. Up to 10 character positions
#: ../partman-partitioning.templates:33001
msgid "biosgrub"
msgstr "biosgrub"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "ctc: Channel to Channel (CTC) or ESCON connection"
msgstr "ctc: ચેનલ ટુ ચેનલ (CTC) અથવા ESCON જોડાણ"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "qeth: OSA-Express in QDIO mode / HiperSockets"
msgstr "qeth: QDIO mode / HiperSockets માં OSA-Express"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "iucv: Inter-User Communication Vehicle - available for VM guests only"
msgstr "iucv: આંતરિક-વપરાશકર્તા સંદેશા વાહન - ફક્ત VM મહેમાનો માટે"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "virtio: KVM VirtIO"
msgstr "virtio: KVM VirtIO"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid "Network device type:"
msgstr "નેટવર્ક ઉપકરણ પ્રકાર:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid ""
"Please choose the type of your primary network interface that you will need "
"for installing the Debian system (via NFS or HTTP). Only the listed devices "
"are supported."
msgstr ""
"મહેરબાની કરી ડેબિયન સિસ્ટમ (NFS અથવા HTTP વડે) સ્થાપન કરવા માટે  તમારા પ્રાથમિક "
"નેટવર્ક  ઇન્ટરફેસનો પ્રકાર પસંદ કરો. ફક્ત યાદી આપેલ ઉપકરણો જ આધાર આપવામાં આવશે."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001
msgid "CTC read device:"
msgstr "CTC વાંચન ઉપકરણ:"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001 ../s390-netdevice.templates:3001
msgid "The following device numbers might belong to CTC or ESCON connections."
msgstr "નીચેનાં ઉપકરણ ક્રમો કદાચ CTC અથવા ESCON જોડાણોને લગતાં છે."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:3001
msgid "CTC write device:"
msgstr "CTC લખાણ ઉપકરણ:"

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001 ../s390-netdevice.templates:8001
#: ../s390-netdevice.templates:12001
msgid "Do you accept this configuration?"
msgstr "તમે આ રૂપરેખાંકન સ્વીકારો છો?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001
msgid ""
"The configured parameters are:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"
msgstr ""
"રૂપરેખાંકન કરેલ વિકલ્પ છે:\n"
" વાંચન ચેનલ  = ${device_read}\n"
" લખાણ ચેનલ = ${device_write}\n"
" પ્રોટોકોલ      = ${protocol}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "No CTC or ESCON connections"
msgstr "કોઇ CTC અથવા ESCON જોડાણો નથી"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "Please make sure that you have set them up correctly."
msgstr "મહેરબાની કરી ખાતરી કરો કે તમે તેમને યોગ્ય રીતે ગોઠવેલ છે."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:6001
msgid "Protocol for this connection:"
msgstr "આ જોડાણ માટે પ્રોટોકોલ:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Device:"
msgstr "ઉપકરણ:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Please select the OSA-Express QDIO / HiperSockets device."
msgstr "મહેરબાની કરી OSA-Express QDIO કાર્ડસ / HiperSockets પસંદ કરો."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:8001
msgid ""
"The configured parameters are:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" layer2   = ${layer2}"
msgstr ""
"રૂપરેખાંકન કરેલ વિકલ્પો છે:\n"
" ચેનલો    = ${device0}, ${device1}, ${device2}\n"
" પોર્ટ    = ${port}\n"
" સ્તર ૨   = ${layer2}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid "No OSA-Express QDIO cards / HiperSockets"
msgstr "કોઇ OSA-Express QDIO કાર્ડસ / HiperSockets નથી"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid ""
"No OSA-Express QDIO cards / HiperSockets were detected. If you are running "
"VM please make sure that your card is attached to this guest."
msgstr ""
"કોઇ OSA-Express QDIO કાર્ડસ / HiperSockets મળ્યાં નહી. જો તમે VM ચલાવતા હોવ તો "
"હોવ તો મહેરબાની કરી ખાતરી કરો કે આ યજમાનમાં કાર્ડ જોડાયેલ છે."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Port:"
msgstr "પોર્ટ:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Please enter a relative port for this connection."
msgstr "મહેરબાની કરી આ જોડાણનો સંબંધિત પોર્ટ દાખલ કરો."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Use this device in layer2 mode?"
msgstr "આ ઉપકરણને સ્તર૨ સ્થિતિમાં વાપરશો?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid ""
"By default OSA-Express cards use layer3 mode. In that mode LLC headers are "
"removed from incoming IPv4 packets. Using the card in layer2 mode will make "
"it keep the MAC addresses of IPv4 packets."
msgstr ""
"મૂળભુત રીતે OSA-એક્સપ્રેસ કાર્ડસ સ્તર ૩ સ્થિતિ ઉપયોગ કરે છે. આ સ્થિતિમાં LLC શિર્ષકો અંદર "
"આવતાં IPv4 પેકેટોમાંથી દૂર કરવામાં આવે છે. સ્તર ૨ ની અંદર કાર્ડ ઉપયોગ કરવાથી તે IPv4 "
"પેકેટોમાં મેક સરનામું રાખી મૂકશે."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid ""
"The configured parameter is:\n"
" peer  = ${peer}"
msgstr ""
"રૂપરેખાંકિત કરેલ વિકલ્પ છે:\n"
" પીઅર  = ${peer}"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "VM peer:"
msgstr "VM પીઅર:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "Please enter the name of the VM peer you want to connect to."
msgstr "મહેરબાની કરી તમે જોડાવા માંગતા હોવ તે VM પીઅરનું નામ દાખલ કરો."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"If you want to connect to multiple peers, separate the names by colons, e."
"g.  tcpip:linux1."
msgstr ""
"જો તમે અનેક પીઅર્સ જોડે જોડાવા માંગતા હોવ તો, નામ ને વિરામચિહ્ન વડે જુદા પાડો, દા.ત.  "
"tcpip:linux1."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"The standard TCP/IP server name on VM is TCPIP; on VIF it's $TCPIP. Note: "
"IUCV must be enabled in the VM user directory for this driver to work and it "
"must be set up on both ends of the communication."
msgstr ""
"VM પર પ્રમાણભૂત TCP/IP સર્વર નામ TCPIP છે; VIF પર તે  $TCPIP છે. નોંધ: આ ડ્રાઇવરનાં "
"કામ કરવા માટે IUCV VM વપરાશ કર્તા ડિરેક્ટરીમાં સક્રિય હોવું જ જોઇએ અને તે સંવાદનાં બંને છેડે "
"સ્થાપિત થયેલું હોવું જ જોઇએ."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "Configure the network device"
msgstr "નેટવર્ક ઉપકરણ રૂપરેખાંકિત કરો"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available devices:"
msgstr "ઉપલબ્ધ ઉપકરણો:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following direct access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"નીચેના ડાયરેક્ટ એક્સેસ સંગ્રહ ઉપકરણો (DASD) ઉપલબ્ધ છે.  મહેરબાની કરી તેમાંથી એક તમે એક "
"સમયે ઉપયોગ કરવા માટે પસંદ કરો."

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "તમે જ્યારે પૂર્ણ કરો ત્યારે યાદીનાં અંતમાં \"પૂર્ણ\" પસંદ કરો."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose device:"
msgstr "ઉપકરણ પસંદ કરો:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a device. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"મહેરબાની કરી ડિસ્ક પસંદ કરો. તમારે સંપૂર્ણ ઉપકરણ ક્રમ, આગળનાં શૂન્યો સાથે સ્પષ્ટ કરવો પડશે."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid device"
msgstr "અયોગ્ય ઉપકરણ"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "અયોગ્ય ઉપકરણ સંખ્યા પસંદ કરવામાં આવી છે."

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001 ../s390-dasd.templates:5001
msgid "Format the device?"
msgstr "ઉપકરણને ફોર્મેટ કરશો?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "The DASD ${device} is already low-level formatted."
msgstr "DASD ${device} પહેલેથી નીચી કક્ષામાં ફોર્મેટ કરેલું છે."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"Please choose whether you want to format again and remove any data on the "
"DASD. Note that formatting a DASD might take a long time."
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid ""
"The DASD ${device} is not low-level formatted. DASD devices must be "
"formatted before you can create partitions."
msgstr ""
"DASD ${device} નીચી કક્ષામાં ફોર્મેટ કરેલ નથી. તમે પાર્ટિશન બનાવી શકો તે પહેલા DASD "
"ઉપકરણ ફોર્મેટ કરેલું હોવું જ જોઇએ."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "The DASD ${device} is in use"
msgstr "વપરાશમાં રહેલ DASD ${device}:"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid ""
"Could not low-level format the DASD ${device} because the DASD is in use.  "
"For example, the DASD could be a member of a mapped device in an LVM volume "
"group."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:7001
msgid "Formatting ${device}..."
msgstr "${device} ફોર્મેટ કરે છે..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:8001
msgid "Configure direct access storage devices (DASD)"
msgstr "ઉપકરણ એક્સેસ સંગ્રહ ઉપકરણો (DASD) રૂપરેખાંકિત કરો"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../zipl-installer.templates:1001
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "હાર્ડડિસ્ક પર ZIPL બૂટ લોડર સ્થાપિત કરો"
